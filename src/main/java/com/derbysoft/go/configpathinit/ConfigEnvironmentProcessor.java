package com.derbysoft.go.configpathinit;

import com.derbysoft.nuke.common.module.json.Json;
import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import static com.derbysoft.nuke.common.module.config.ApplicationConfiguration.*;

public class ConfigEnvironmentProcessor implements EnvironmentPostProcessor, Ordered {

    private String activeFileNameKey = null;

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        if (!isEnable(application.getMainApplicationClass())) {
            System.out.println(String.format("EnableActiveConfig is not enable on %s", application.getMainApplicationClass().getName()));
            return;
        }
        resolveActiveFileName(application);
        if (activeFileNameKey == null) {
            throw new IllegalArgumentException("fileNameKey is required in @EnableActiveConfig on " + application.getMainApplicationClass().getName());
        }
        initConfigPath();
    }

    private void resolveActiveFileName(SpringApplication application) {
        Class<?> applicationClass = application.getMainApplicationClass();
        EnableActiveConfig enableConfig = AnnotationUtils.findAnnotation(applicationClass, EnableActiveConfig.class);
        activeFileNameKey = (String) AnnotationUtils.getValue(enableConfig, "fileNameKey");
    }

    private boolean isEnable(Class<?> applicationClass) {
        return AnnotationUtils.isAnnotationDeclaredLocally(EnableActiveConfig.class, applicationClass);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 40;
    }

    private void initConfigPath() {
        Properties properties = System.getProperties();
        Map envMap = System.getenv();
        Object activeConfig;
        System.out.println(String.format("envMap:{%s}", envMap == null ? StringUtils.EMPTY : Json.getDefault().toJsonString(envMap)));
        if (Objects.isNull(envMap)
                || Objects.isNull(activeConfig = envMap.get(activeFileNameKey))) {
            System.out.println("fileNameKey of EnableActiveConfig is not setting on environment!");
            return;
        }
        String activeApplicationConfig = CLASSPATH_CONFIG_PREFIX + activeConfig;
        String configNames = Joiner.on(",").join(Arrays.asList(SYSTEM_CONFIG, DEFAULT_CONFIG, APPLICATION_CONFIG, activeApplicationConfig, REMOTE_CONFIG));
        properties.setProperty(APPLICATION_CONFIGS_PROPERTY, configNames);
        System.out.println(String.format("the config[%s]  has been appended  successfully!", activeConfig));
    }


}
